export const setName=(name)=> {
  return {
    type: 'SET_NAME',
    payload: name
  }
}
export const setId=(id)=> {
  return {
    type: 'SET_ID',
    payload: id
  }
}
export const setSocket=(socket)=> {
  return {
    type: 'SET_SOCKET',
    payload: socket
  }
}
export const setReady=(val)=> {
  return {
    type: 'SET_READY',
    payload: val
  }
}
export const setPlaying=(val)=> {
  return {
    type: 'SET_PLAYING',
    payload: val
  }
}
export const setRole=(val)=> {
  return {
    type: 'SET_ROLE',
    payload: val
  }
}
export const setColor=(val)=> {
  return {
    type: 'SET_COLOR',
    payload: val
  }
}
