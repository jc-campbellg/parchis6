export const addMessage=(name,message,type)=> {
  return {
    type: 'ADD_MESSAGE',
    payload: {
      name: name,
      message: message,
      type: type
    }
  }
}
