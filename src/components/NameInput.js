import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addMessage} from '../actions/chatActions';

class NameInput extends Component{
  constructor(props){
    super(props);
    this.state = {
      name: '',
      gameId: ''
    };
  };
  submitHandler=(e)=>{
    e.preventDefault();
    let name = this.state.name.trim();
    let gameId = this.state.gameId.trim();
    if (name.length > 3 && gameId.length > 3){
      if (name.match(/^[A-Za-z0-9]+$/) && gameId.match(/^[A-Za-z0-9]+$/)){
        if (name.length < 11 && gameId.length < 11){
          //Send Name
          this.props.socket.emit('getOnline',{name:name,room:gameId});
        } else {
          //Name is too long
          this.props.dispatch(addMessage('Server','Nombre o ID es muy largo','error'));
        }
      } else {
        //Error Only use letters and numbers
        this.props.dispatch(addMessage('Server','Solo use numeros y/o letras','error'));
      }
    } else {
      this.props.dispatch(addMessage('Server','Minimo 3 caracteres por campo','error'));
    }
  };
  typingName=(e)=>{
    this.setState({
      name: e.target.value
    });
  };
  typingGameId=(e)=>{
    this.setState({
      gameId: e.target.value
    });
  };
  render() {
    if (this.props.ready){
      return null;
    } else {
      return(
        <form onSubmit={this.submitHandler}>
          <input
            type="text"
            placeholder="Nombre"
            autoComplete="off"
            onChange={this.typingName}
          />
          <input
            type="text"
            placeholder="ID de Juego"
            autoComplete="off"
            onChange={this.typingGameId}
          />
          <br/>
          <input
            type="submit"
            value="OK"
          />
        </form>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    name: state.user.name,
    id: state.user.id,
    socket: state.user.socket,
    ready: state.user.ready
  }
}

export default connect(mapStateToProps)(NameInput);
