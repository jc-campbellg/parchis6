import React, { Component } from 'react';
import Message from './subcomponents/Message';
import {connect} from 'react-redux';
import socketIOClient from "socket.io-client";
import {setId, setName, setSocket, setReady, setRole, setPlaying} from '../actions/userActions';
import {addMessage} from '../actions/chatActions';

class ChatHandler extends Component{
  componentDidMount(){
    this.fullHeight = document.getElementsByClassName('chatSpace').clientHeight;
    this.socket = socketIOClient("http://parchis6.disenadospordios.com:4000");
    //this.socket = socketIOClient("192.168.0.19:4000");
    //Hanlde messages
    this.socket.on('id',this.onlineSetID);
    this.socket.on('chat',this.onlineChat);
    this.socket.on('ready',this.onlineReady);
    this.socket.on('role',this.onlineRole);
    this.socket.on('playing',this.onlinePlaying);
  };
  onlinePlaying = (data) =>{
    this.props.dispatch(setPlaying(data));
  };
  onlineRole = (data) =>{
    this.props.dispatch(setRole(data));
  };
  onlineSetID = (data) =>{
    this.props.dispatch(setId(data));
    this.props.dispatch(setSocket(this.socket));
    this.props.dispatch(addMessage('Server','Bienvenido','suceed'));
  };
  onlineChat = (data) =>{
    this.props.dispatch(addMessage(data.name,data.message,data.type));
  };
  onlineReady = (data, fn) =>{
    this.props.dispatch(setReady(true));
    this.props.dispatch(setName(data));
    fn(data);
  };
  render() {
    const messages = this.props.messages;
    const chatHistory = messages.slice(0).reverse().map((msg, index)=>{
      return(
        <Message key={msg.key} name={msg.name} type={msg.type} message={msg.message}/>
      );
    });
    return(
      <ul className="chatBox">
        {chatHistory}
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    socket: state.user.socket
  }
}

export default connect(mapStateToProps)(ChatHandler);
