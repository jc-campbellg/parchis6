import React, { Component } from 'react';
import BoardSVG from './svg/BoardSVG';
import ColorPicker from './ColorPicker';
import {connect} from 'react-redux';

class GameHandler extends Component{
  render() {
    if (this.props.ready){
      return(
        <div>
          {this.props.playing ? null : <ColorPicker />}
          <BoardSVG/>
        </div>
      );
    } else {
      return null;
    }

  }
}

const mapStateToProps = (state) => {
  return {
    ready: state.user.ready,
    playing: state.user.playing
  }
}

export default connect(mapStateToProps)(GameHandler);
