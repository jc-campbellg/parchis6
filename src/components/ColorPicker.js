import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setColor, setPlaying} from '../actions/userActions';

class ColorPicker extends Component{
  constructor(props){
    super(props);
    this.state = {
      colors: []
    };
  };
  componentDidMount(){
    this.props.socket.on('colors',this.onlineColors);
  };
  onlineColors = (data) =>{
    this.setState({
      colors: data
    });
  };
  selectColor=(e)=>{
    this.props.dispatch(setColor(e.target.value));
    this.props.socket.emit('selectColor',e.target.value);
  };
  render() {
    var info;
    switch (this.props.role) {
      case 'admin':
        info="Eres el administrador de la mesa, seleciona un color.";
        break;
      case 'player':
        info="Seleciona un color.";
        break;
      default:
        info="Solo eres espectador de la mesa";
    }
    const colors = this.state.colors.map((color, index)=>{
      return(
        <button key={color} value={color} className={"ColorPicker "+color} onClick={this.selectColor}></button>
      );
    });

    return(
      <div>
        <p className="ColorPickerP"> {info} </p>
        {colors}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    socket: state.user.socket,
    role: state.user.role,
    color: state.user.color
  }
}

export default connect(mapStateToProps)(ColorPicker);
