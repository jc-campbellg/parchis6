import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addMessage} from '../actions/chatActions';

class Chat extends Component{
  constructor(props){
    super(props);
    this.state = {
      value: ''
    };
  };
  typing = (e)=>{
    this.setState({
      value: e.target.value
    });
  };
  submitHandler=(e)=>{
    e.preventDefault();
    if (this.props.ready) {
      let name = this.props.name;
      let msg = this.state.value;
      this.props.socket.emit('chat',{name:name,message:msg,type:this.props.color});
    } else {
      this.props.dispatch(addMessage('Server','Inicia un juego para poder enviar mensajes','error'));
    }
    this.setState({
      value: ''
    });
  };
  render() {
    return(
      <form className="Chat" onSubmit={this.submitHandler}>
        <input
          type="text"
          placeholder="Escribe mensaje"
          onChange={this.typing}
          value={this.state.value}
        />
        <input
          type="submit"
          value="↵"
        />
      </form>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    name: state.user.name,
    socket: state.user.socket,
    ready: state.user.ready,
    color: state.user.color
  }
}

export default connect(mapStateToProps)(Chat);
