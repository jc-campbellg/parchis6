import React, { Component } from 'react';

class BoardSVG extends Component{
  render() {
    return(
      <svg className="BoardSVG" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 449.1 457.1">
        <path fill="#3C2415" d="M75.2 79l-60 103.9v103.2l60 104 89.3 51.6h120.1l89.3-51.6 60-104V182.9L373.9 79l-89.3-51.6H164.5z"/>
        <path fill="#A97C50" stroke="#3E2715" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M75.2 67l-60 104v103.2l60 103.9 89.3 51.6h120.1l89.3-51.6 60-103.9V171l-60-104-89.3-51.6H164.5z"/>
        <path fill="#8B5E3C" d="M79.5 76.4l-57.3 99.3v98.6l57.3 99.2 85.3 49.3h114.6l85.4-49.3 57.3-99.2v-98.6l-57.3-99.3-85.4-49.2H164.8z"/>
        <g>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M180.3 152.5h23.1v-15.1h-31.9zM171.5 137.4h31.9v-15.1h-37.3v5.7zM166.1 107.2h37.3v15.1h-37.3zM166.1 77h37.3v15.1h-37.3zM166.1 61.8h37.3v15.1h-37.3z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 46.7h37.3v15.1h-37.3zM166.1 31.6h37.3v15.1h-37.3zM264 152.5h-23.2v-15.1h31.9zM272.7 137.4h-31.9v-15.1h37.3v5.7zM240.8 107.2h37.3v15.1h-37.3zM240.8 77h37.3v15.1h-37.3zM240.8 61.8h37.3v15.1h-37.3z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M240.8 46.7h37.3v15.1h-37.3zM240.8 31.6h37.3v15.1h-37.3zM138.4 225l11.6-20.1-13.1-7.5L121 225z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M121 225l15.9-27.6-13.1-7.6-18.6 32.3 4.9 2.9z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M123.873 189.835l-18.65 32.302-13.076-7.55 18.65-32.302zM97.709 174.696l-18.65 32.301-13.077-7.55 18.65-32.301zM84.583 167.101l-18.65 32.302-13.076-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M71.544 159.556l-18.65 32.302-13.076-7.55 18.65-32.302zM58.369 152.048l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M180.3 152.5l-11.6 20.1-13.1-7.6 15.9-27.6zM171.5 137.4L155.6 165l-13.1-7.5 18.7-32.4 4.9 2.9z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M129.441 149.959l18.65-32.302 13.077 7.55-18.65 32.302zM103.228 134.742l18.65-32.302 13.077 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M90.183 127.227l18.65-32.302 13.077 7.55-18.65 32.302zM77.051 119.662l18.65-32.302 13.077 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M64.006 112.147l18.65-32.302 13.077 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M305.8 225l-11.6-20.1 13.1-7.5 15.9 27.6z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M323.2 225l-15.9-27.6 13.1-7.6 18.7 32.3-5 2.9z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M333.514 182.231l18.65 32.302-13.076 7.55-18.65-32.302zM359.69 167.151l18.65 32.302-13.076 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M372.686 159.55l18.65 32.302-13.077 7.55-18.65-32.302zM385.817 151.985l18.65 32.301-13.076 7.55-18.65-32.301z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M398.863 144.47l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M264 152.5l11.5 20.1 13.1-7.6-15.9-27.6z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M272.7 137.4l15.9 27.6 13.1-7.5-18.7-32.4-4.9 2.9z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M301.76 157.486l-18.65-32.302 13.077-7.55 18.65 32.302zM327.839 142.397l-18.65-32.302 13.076-7.55 18.65 32.301zM340.964 134.802l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M354.003 127.257l-18.65-32.302 13.077-7.55 18.65 32.302zM367.129 119.663l-18.65-32.302 13.076-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M264 297.5l11.5-20.1 13.1 7.5-15.9 27.7z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M272.7 312.6l15.9-27.7 13.1 7.6-18.7 32.3-4.9-2.8z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M314.814 300.004l-18.65 32.302-13.076-7.55 18.65-32.302zM340.979 315.143l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M354.017 322.688l-18.65 32.302-13.076-7.55 18.65-32.302zM367.143 330.283l-18.65 32.301-13.077-7.55 18.65-32.301z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M380.182 337.827l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M305.8 225l-11.6 20 13.1 7.6 15.9-27.6z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M323.2 225l-15.9 27.6 13.1 7.6 18.7-32.4-5-2.8z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M320.374 260.149l18.65-32.302 13.077 7.55-18.65 32.302zM346.551 275.229l18.65-32.302 13.077 7.55-18.65 32.302zM359.633 282.88l18.65-32.302 13.076 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M372.678 290.395l18.65-32.302 13.076 7.55-18.65 32.302zM385.81 297.96l18.65-32.302 13.076 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M264 297.5h-23.2v15.1h31.9zM272.7 312.6h-31.9v15.1h37.3V322z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M240.8 327.7h37.3v15.1h-37.3zM240.8 357.9h37.3V373h-37.3zM240.8 373h37.3v15.1h-37.3zM240.8 388.1h37.3v15.1h-37.3z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M240.8 403.2h37.3v15.1h-37.3zM180.3 297.5h23.1v15.1h-31.9zM171.5 312.6h31.9v15.1h-37.3V322z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 327.7h37.3v15.1h-37.3zM166.1 357.9h37.3V373h-37.3zM166.1 373h37.3v15.1h-37.3zM166.1 388.1h37.3v15.1h-37.3z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 403.2h37.3v15.1h-37.3zM138.4 225l11.6 20-13.1 7.6L121 225z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M121 225l15.9 27.6-13.1 7.6-18.6-32.4 4.9-2.8z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M110.733 267.705l-18.65-32.302 13.077-7.55 18.65 32.302zM84.569 282.844l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M71.53 290.389l-18.65-32.302 13.076-7.55 18.65 32.302zM58.404 297.983l-18.65-32.301 13.077-7.55 18.65 32.301z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M45.315 305.442l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M180.3 297.5l-11.6-20.1-13.1 7.5 15.9 27.7zM171.5 312.6l-15.9-27.7-13.1 7.6 18.7 32.3 4.9-2.8z"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M142.495 292.471l18.65 32.302-13.077 7.55-18.65-32.302zM116.368 307.638l18.65 32.302-13.077 7.55-18.65-32.302zM103.236 315.203l18.65 32.302-13.076 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M90.191 322.718l18.65 32.302-13.076 7.55-18.65-32.302zM77.06 330.283l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M26.7 176.8l83.4 48.2-83.4 48.1z"/>
          <path fill="#107BA3" d="M41.3 190.1l54.1 31.3c2.8 1.6 2.8 5.7 0 7.3L41.3 260c-2.8 1.6-6.3-.4-6.3-3.6v-62.5c0-3.4 3.5-5.4 6.3-3.8z"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M417.6 176.8L334.1 225l83.5 48.1z"/>
          <path fill="#9F2067" d="M402.9 190.1l-54.1 31.3c-2.8 1.6-2.8 5.7 0 7.3l54.1 31.3c2.8 1.6 6.3-.4 6.3-3.6v-62.5c0-3.4-3.5-5.4-6.3-3.8z"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M278.1 31.6V128l83.4-48.2z"/>
          <path fill="#A53C23" d="M282.3 50.9v62.5c0 3.2 3.5 5.2 6.3 3.6l54.1-31.3c2.8-1.6 2.8-5.7 0-7.3l-54.1-31.3c-2.8-1.4-6.3.6-6.3 3.8z"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 31.6V128L82.7 79.8z"/>
          <path fill="#9F1D20" d="M161.9 50.9v62.5c0 3.2-3.5 5.2-6.3 3.6l-54.1-31.3c-2.8-1.6-2.8-5.7 0-7.3l54.1-31.3c2.8-1.4 6.3.6 6.3 3.8z"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M278.1 418.3V322l83.4 48.2z"/>
          <path fill="#0A5A30" d="M282.3 399v-62.5c0-3.2 3.5-5.2 6.3-3.6l54.1 31.3c2.8 1.6 2.8 5.7 0 7.3l-54.1 31.3c-2.8 1.5-6.3-.5-6.3-3.8z"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 418.3V322l-83.4 48.2z"/>
          <path fill="#B1AB34" d="M161.9 399v-62.5c0-3.2-3.5-5.2-6.3-3.6l-54.1 31.3c-2.8 1.6-2.8 5.7 0 7.3l54.1 31.3c2.8 1.5 6.3-.5 6.3-3.8z"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 137.4h37.3v15.1h-37.3zM180.3 152.5l41.8 72.5 41.9-72.5zM203.4 122.3h37.3v15.1h-37.3zM203.4 107.2h37.3v15.1h-37.3zM203.4 92.1h37.3v15.1h-37.3zM203.4 77h37.3v15.1h-37.3zM203.4 61.8h37.3v15.1h-37.3z"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 46.7h37.3v15.1h-37.3z"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M264 152.5L222.1 225h83.7z"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M288.615 164.967l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M301.66 157.452l18.65 32.302-13.077 7.55-18.65-32.302zM314.791 149.887l18.65 32.302-13.076 7.55-18.65-32.302zM327.923 142.322l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M340.968 134.807l18.65 32.302-13.077 7.55-18.65-32.302zM354.1 127.242l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M367.144 119.727l18.65 32.302-13.076 7.55-18.65-32.302zM314.8 149.941l-18.65-32.302 13.076-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 92.1h37.3v15.1h-37.3z"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M138.4 225h83.7l-41.8-72.5z"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M168.71 172.548l-18.65 32.302-13.077-7.55 18.65-32.302zM155.584 164.954l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M142.495 157.496l-18.65 32.301-13.077-7.55 18.65-32.301z" stroke-width=".99998"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M129.456 149.951l-18.65 32.302-13.076-7.55 18.65-32.302zM116.33 142.357l-18.65 32.301-13.076-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M103.292 134.812l-18.65 32.302-13.077-7.55 18.65-32.302zM90.166 127.217l-18.65 32.302-13.077-7.55 18.65-32.302zM110.748 182.24l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M180.3 297.5l41.8-72.5h-83.7z"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M155.57 284.991l-18.65-32.301 13.076-7.55 18.65 32.301z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M142.53 292.536l-18.65-32.302 13.077-7.55 18.65 32.302zM129.355 300.044l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M116.316 307.589l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M103.277 315.133l-18.65-32.302 13.077-7.55 18.65 32.302zM90.152 322.728l-18.65-32.302 13.076-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M77.113 330.272l-18.65-32.301 13.076-7.55 18.65 32.301zM129.45 299.986l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M222.1 225l-41.8 72.5H264zM203.4 297.5h37.3v15.1h-37.3zM203.4 312.6h37.3v15.1h-37.3z"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 327.7h37.3v15.1h-37.3zM203.4 342.8h37.3v15.1h-37.3z"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 357.9h37.3V373h-37.3zM203.4 373h37.3v15.1h-37.3zM203.4 388.1h37.3v15.1h-37.3zM240.8 342.8h37.3v15.1h-37.3z"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M305.8 225h-83.7l41.9 72.5z"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M307.358 252.576l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M320.397 260.12l-18.65 32.302-13.077-7.55 18.65-32.302zM333.523 267.715l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M346.561 275.26l-18.65 32.302-13.076-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M359.6 282.804l-18.65 32.302-13.076-7.55 18.65-32.302zM372.726 290.399l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M385.765 297.943l-18.65 32.302-13.077-7.55 18.65-32.302zM333.506 267.714l18.65-32.302 13.077 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 31.6h37.3v15.1h-37.3z"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M240.8 92.1h37.3v15.1h-37.3z"/>
          <path fill="#ED1C24" stroke="#9F1D20" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M116.31 142.394l18.65-32.302 13.076 7.55-18.65 32.302z" stroke-width=".99998"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M346.56 174.716l18.65 32.302-13.077 7.55-18.65-32.302z" stroke-width=".99998"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M327.94 307.599L309.29 339.9l-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M166.1 342.8h37.3v15.1h-37.3z"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M97.695 275.25l-18.65-32.302 13.076-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#F26522" stroke="#A53C23" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M380.226 112.076l18.65 32.301-13.077 7.55-18.65-32.301z" stroke-width=".99998"/>
          <path fill="#00AEEF" stroke="#107BA3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M77.127 119.673l-18.65 32.302-13.077-7.55 18.65-32.302z" stroke-width=".99998"/>
          <path fill="#FFF200" stroke="#B1AB34" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M63.987 337.867l-18.65-32.302 13.077-7.55 18.65 32.302z" stroke-width=".99998"/>
          <path fill="#00A651" stroke="#0A5A30" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M203.4 403.2h37.3v15.1h-37.3z"/>
          <path fill="#EC008C" stroke="#9F2067" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M398.94 305.452l-18.65 32.301-13.076-7.55 18.65-32.301z" stroke-width=".99998"/>
          <circle fill="#9F1D20" cx="184.8" cy="99.6" r="4.5"/>
          <circle fill="#107BA3" cx="94.9" cy="194.6" r="4.5"/>
          <circle fill="#B1AB34" cx="132.2" cy="320.1" r="4.5"/>
          <circle fill="#0A5A30" cx="259.5" cy="350.2" r="4.5"/>
          <circle fill="#9F2067" cx="349.3" cy="255.3" r="4.5"/>
          <circle fill="#A53C23" cx="312" cy="130" r="4.5"/>
        </g>
      </svg>
    );
  }
}

export default BoardSVG;
