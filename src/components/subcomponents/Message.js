import React, { Component } from 'react';

class Message extends Component{
  render() {
    return(
      <li><span className={this.props.type}>{this.props.name}:</span>{this.props.message}</li>
    );
  }
}


export default (Message);
