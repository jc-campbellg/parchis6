export default function reducer(state={
  name: '',
  socket: '',
  id: '',
  role: '',
  ready: false,
  playing: false,
  color: 'grey'
},action) {
  switch (action.type) {
    case "SET_NAME":
      return {...state, name:action.payload};
    case "SET_ID":
      return {...state, id:action.payload};
    case "SET_SOCKET":
      return {...state, socket:action.payload};
    case "SET_READY":
      return {...state, ready:action.payload};
    case "SET_PLAYING":
      return {...state, playing:action.payload};
    case "SET_ROLE":
      return {...state, role:action.payload};
    case "SET_COLOR":
      return {...state, color:action.payload};
    default:
      return state;
  }
}
