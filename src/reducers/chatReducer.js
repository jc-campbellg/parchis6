export default function reducer(state={
  messages:[]
},action) {
  switch (action.type) {
    case "ADD_MESSAGE":
      let newMSG = action.payload;
      newMSG.key = state.messages.length;
      return {
        ...state,
        messages:[...state.messages ,newMSG]
      };
    default:
      return state;
  }
}
