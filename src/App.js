import React, { Component } from 'react';
import NameInput from './components/NameInput';
import GameHandler from './components/GameHandler';
import Chat from './components/Chat';
import ChatHandler from './components/ChatHandler';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="gameSpace">
          <div className="gameBox">
            <GameHandler />
          </div>
        </div>
        <div className="chatSpace">
          <NameInput />
          <ChatHandler />
          <Chat />
        </div>
      </div>
    );
  }
}

export default App;
